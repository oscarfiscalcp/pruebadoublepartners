<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AcoountController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
/* Route::apiResource ('accounts', [ App\Http\Controllers\api\AcoountController::class, 'store']);
 */

 /* ROUTE ACCOUNT */
Route::post('/accounts', [ App\Http\Controllers\api\AcoountController::class, 'store']);
Route::get('/accounts', [ App\Http\Controllers\api\AcoountController::class, 'index']);
Route::get('/accounts/{account}', [ App\Http\Controllers\api\AcoountController::class, 'show']);
Route::put('/accounts/{account}', [ App\Http\Controllers\api\AcoountController::class, 'update']);
Route::delete('/accounts/{account}', [ App\Http\Controllers\api\AcoountController::class, 'destroy']);

/* ROUTE ORDER */
Route::post('/order', [ App\Http\Controllers\api\OrderController::class, 'store']);
Route::get('/order', [ App\Http\Controllers\api\OrderController::class, 'index']);
Route::get('/order/{order}', [ App\Http\Controllers\api\OrderController::class, 'show']);
Route::put('/order/{order}', [ App\Http\Controllers\api\OrderController::class, 'update']);
Route::delete('/order/{order}', [ App\Http\Controllers\api\OrderController::class, 'destroy']);