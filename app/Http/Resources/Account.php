<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Account extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [  
             
            'data'=>[
           'type'=>'accounts',
        'account_id'=>$this->id,
           'attributes'=>[
               'name'=>$this->name,
                'email'=>$this->email,
                'phone'=>$this->phone,
                         ]
         ]
                
                
                ];
    }
}
