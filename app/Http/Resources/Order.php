<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [  
             
            'data'=>[
           'type'=>'order',
        'order_id'=>$this->id,
           'attributes'=>[
               'product'=>$this->product,
                'quantity'=>$this->quantity,
                'value'=>$this->value,
                'total'=>$this->total,
                'account_id'=>new Account($this->account),
              
                         ]
         ]
                
                
                ];
    }
}
