<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Http\Requests\Account as AccountRequests;
use App\Http\Resources\Account as AccountResources;
use App\Http\Resources\AccountCollection;


class AcoountController extends Controller
{
    protected $account;

    public function __construct (Account $account)
    {
        $this->account = $account;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $account = Account::all();

        return new AccountCollection ($account);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountRequests $request)
    {
        $account = $this->account->create($request->all());

        return response()->json(new AccountResources($account),201);
   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        return new AccountResources($account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */

    public function update(AccountRequests $request, Account $account)
    {  
        $account->update($request->all());

        return response()->json(new AccountResources($account),200);
    }
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->delete();
        return response()->json(['message'=>'Account deleted successfully!']);
    }
}
