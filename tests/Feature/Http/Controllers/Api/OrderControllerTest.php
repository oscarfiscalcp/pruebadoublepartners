<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Order;
use App\Models\Account;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_order_index()
    {
        $this->withoutExceptionHandling();

        Order::factory()->create();

        $response = $this->get('/api/order');

        $response->assertOk();
        $order = Order::all();

        $response->assertJson([
            'data' => [
                [
                    'data' => [
                        'type' => 'order',
                        'order_id' => $order->first()->id,
                        'attributes' => [
                            'product' => $order->first()->product,
                            'quantity' => $order->first()->quantity,
                            'value' => $order->first()->value,
                            'total' => $order->first()->total,
                     
                        ]
                    ]
                ]
            ],

        ]);
    }
    public function test_order_can_be_created()
    {
        $this->withoutExceptionHandling();
        $response = $this->postJson('/api/order', [
            'product' => 'pizza',
            'quantity' => '3',
            'value' => '10',
            'total' => '30',
            'account_id'=>Account::factory()->create()->id,
        ])->assertCreated();

        $order = Order::first();

        $this->assertCount(1, Order::all());

        $this->assertEquals('pizza', $order->product);
        $this->assertEquals('3', $order->quantity);
        $this->assertEquals('10', $order->value);
        $this->assertEquals('30', $order->total);
      
       


        $response->assertJson([
            'data' => [
                'type' => 'order',
                'order_id' => $order->id,
                'attributes' => [
                    'product' => $order->product,
                    'quantity' => $order->quantity,
                    'value' => $order->value,
                    'account_id'=>[
                        'data'=>[
                        'account_id'=>$order->account->id,
                        ]
                        ],
                ],
               
            ],
          
        ]);
    }

    public function test_retrieve_and_list_order()
    {
        $this->withoutExceptionHandling();

        $order = Order::factory()->create();

        $response = $this->getJson('/api/order/' . $order->id);
        $response->assertOk();
        
        $response->assertJson([
            'data' => [
                'type' => 'order',
                'order_id' => $order->id,
                'attributes' => [
                    'product' => $order->product,
                    'quantity' => $order->quantity,
                    'value' => $order->value,
                    'total' => $order->total,
                ]
            ],
        ]);
        
    }

    public function test_to_update_order()
                {
                    $this->withoutExceptionHandling();
                    $order = Order::factory()->create();
                    $response = $this->putJson('/api/order/' . $order->id, [
                        'product' => 'hamburger',
                        'quantity' => '2',
                        'value' => '10',
                        'total' => '20',
                        
                    ]);
                    $response->assertOk();
                    $this->assertCount(1,Order::all());   
                    $order = $order->fresh();
                  
                    $this->assertEquals('hamburger', $order->product);
                    $this->assertEquals('2', $order->quantity);
                    $this->assertEquals('10', $order->value);
                    $this->assertEquals('20', $order->total);
                 

                    $response->assertJson([
                        'data' => [
                            'type' => 'order',
                            'order_id' => $order->id,
                            'attributes' => [
                                'product' => $order->product,
                                'quantity' => $order->quantity,
                                'value' => $order->value,
                                'total' => $order->total,
                            ]
                        ],
                    ]);

                
                }

                public function test_to_delete_order()
                {
                    $this->withoutExceptionHandling();
                    $order = Order::factory()->create();
                    $response = $this->deleteJson('/api/order/' . $order->id);
                    $response->assertOk();
                    $this->assertCount(0,Order::all());   
            
                    $response->assertJson([
            
                        'message' => 'Account deleted successfully!',
                    ]);
                }
}
