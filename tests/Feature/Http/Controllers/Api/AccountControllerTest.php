<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Account;

class AccountControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /* list account */
    public function test_account_index()
    {
        $this->withoutExceptionHandling();

        Account::factory()->create();

        $response = $this->get('/api/accounts');

        $response->assertOk();
        $account = Account::all();

        $response->assertJson([
            'data' => [
                [
                    'data' => [
                        'type' => 'accounts',
                        'account_id' => $account->first()->id,
                        'attributes' => [
                            'name' => $account->first()->name,
                            'email' => $account->first()->email,
                            'phone' => $account->first()->phone,
                        ]
                    ]
                ]
            ],

        ]);
    }


    /*  test create account */
    public function test_account_can_be_created()
    {
        $this->withoutExceptionHandling();
        $response = $this->postJson('/api/accounts', [
            'name' => 'test',
            'email' => 'example@gmail.com',
            'phone' => '1234567890',
        ])->assertCreated();

        $account = Account::first();

        $this->assertCount(1, Account::all());

        $this->assertEquals('test', $account->name);
        $this->assertEquals('example@gmail.com', $account->email);
        $this->assertEquals('1234567890', $account->phone);


        $response->assertJson([
            'data' => [
                'type' => 'accounts',
                'account_id' => $account->id,
                'attributes' => [
                    'name' => $account->name,
                    'email' => $account->email,
                    'phone' => $account->phone,
                ]
            ],
        ]);
    }

    public function test_retrieve_and_list_account()
    {
        $this->withoutExceptionHandling();

        $account = Account::factory()->create();

        $response = $this->getJson('/api/accounts/' . $account->id);
        $response->assertOk();
        
    }

    public function test_to_update_account()
                {
                    $this->withoutExceptionHandling();
                    $account = Account::factory()->create();
                    $response = $this->putJson('/api/accounts/' . $account->id, [
                        'name' => 'test12',
                        'email' => 'example12@gmail.com',
                        'phone' => '12345678909',
                    ]);
                    $response->assertOk();
                    $this->assertCount(1,Account::all());   
                    $account = $account->fresh();
                  
                    $this->assertEquals('test12', $account->name);
                    $this->assertEquals('example12@gmail.com', $account->email);
                    $this->assertEquals('12345678909', $account->phone);

                    $response->assertJson([
                        'data' => [
                            'type' => 'accounts',
                            'account_id' => $account->id,
                            'attributes' => [
                                'name' => $account->name,
                                'email' => $account->email,
                                'phone' => $account->phone,
                            ]
                        ],
                    ]);

                
                }


    public function test_to_delete_account()
    {
        $this->withoutExceptionHandling();
        $account = Account::factory()->create();
        $response = $this->deleteJson('/api/accounts/' . $account->id);
        $response->assertOk();
        $this->assertCount(0,Account::all());   

        $response->assertJson([

            'message' => 'Account deleted successfully!',
        ]);
    }

}
