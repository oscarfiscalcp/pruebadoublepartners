<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Account;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'product' => $this->faker->word,
            'quantity' => $this->faker->randomNumber(2),
            'value' => $this->faker->randomNumber(2),
            'total' => $this->faker->randomNumber(2),
            'account_id' =>  Account::factory()->create(),
        ];
    }
}
